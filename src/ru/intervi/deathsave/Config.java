package ru.intervi.deathsave;

import java.io.File;

import org.bukkit.ChatColor;

import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.utils.Utils;

public class Config {
	public Config() {
		load();
	}
	
	public boolean enable = true;
	public boolean cons = true; //выводить информацию в консоль
	public boolean noreg = true; //не делать сундук в чужих регионах
	public boolean nokeepinv = true; //не действовать при вкл. сохранении инвентаря
	public boolean skipperm = true; //не проверять разрешения
	public String date = "YYYY-MM-dd/HH:mm:ss"; //формат даты
	public String mess = "ты умер на %loc%"; //сообщение игроку
	public String noperm = "нет прав";
	public String help = "/ds reload - перезагрузить конфиг";
	public String reload = "конфиг перезагружен";
	public String noreglist[] = null; //в каких регионах сундук делать нельзя
	public String noblock[] = null; //какие блоки не заменять
	public String noworlds[] = null; //миры исключения
	
	private String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public void load() {
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "DeathSave";
		File dir = new File(path);
		if (!dir.isDirectory()) dir.mkdirs();
		path += sep + "config.yml";
		File file = new File(path);
		if (!file.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/config.yml"), file);
		
		ConfigLoader config = null;
		try {
			config = new ConfigLoader(file, false);
		} catch(Exception e) {e.printStackTrace();}
		if (config == null) return;
		
		enable = config.getBoolean("enable");
		cons = config.getBoolean("cons");
		noreg = config.getBoolean("noreg");
		nokeepinv = config.getBoolean("nokeepinv");
		date = config.getString("date");
		mess = color(config.getString("mess"));
		noperm = color(config.getString("noperm"));
		help = color(config.getString("help"));
		reload = color(config.getString("reload"));
		noreglist = config.getStringArray("noreglist");
		noblock = config.getStringArray("noblock");
		skipperm = config.getBoolean("skipperm");
		noworlds = config.getStringArray("noworlds");
	}
}
