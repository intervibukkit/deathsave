package ru.intervi.deathsave;

import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.ProtectionQuery;
import com.sk89q.worldguard.protection.managers.RegionManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.entity.Player;
import org.bukkit.block.Chest;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.Bukkit;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main extends JavaPlugin implements Listener {
	private Config conf = new Config();
	private SimpleDateFormat d = new SimpleDateFormat(conf.date); //формат даты
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	private WorldGuardPlugin getWorldGuard() { //получить WorldGuard
	    Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) plugin;
	}
	
	private String getLoc(Location loc) { //получить локацию в виде строки
        return loc.getWorld().getName() + ": " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ();
    }

    private Material getNoLegacy(Material material) {
		String key = material.toString();
		if (key.contains("LEGACY")) key = key.substring(key.indexOf('_') + 1);
		return Material.getMaterial(key);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDeath(PlayerDeathEvent event) {
		if (!conf.enable) return;
		Player player = event.getEntity();
		if (!conf.skipperm && (player.hasPermission("ds.exempt") || !player.hasPermission("ds.use"))) return;
		Location loc = player.getLocation();
		if (conf.noworlds != null && conf.noworlds.length > 0) {
			for (String w : conf.noworlds) if (w.equals(loc.getWorld().getName())) return; //проверка на миры иключения
		}
		//проверка правила
		if (conf.nokeepinv && loc.getWorld().getGameRuleValue("keepInventory").equalsIgnoreCase("true")) return;
		Block block = loc.getBlock(); //получение блоков
		Block block2 = block.getRelative(0, 1, 0);
		WorldGuardPlugin wg = getWorldGuard(); //получение плагина WorldGuard для проверок
		if (wg != null) {
			ProtectionQuery query = wg.createProtectionQuery();
			if (
					conf.noreg && (
							!query.testBlockPlace(player, block.getLocation(), getNoLegacy(block.getType())) ||
							!query.testBlockPlace(player, block2.getLocation(), getNoLegacy(block2.getType()))
					)
			) {
				return;
			}
			if (conf.noreglist != null && conf.noreglist.length > 0) { //проверка на запрещенные регионы
				RegionManager manager = WorldGuard.getInstance().getPlatform()
						.getRegionContainer().get(new BukkitWorld(loc.getWorld()));
				if (manager != null) {
					List<String> rglist = manager.getApplicableRegionsIDs(
							BlockVector3.at(loc.getX(), loc.getZ(), loc.getY())
					);
					for (String nrg : conf.noreglist) {
						for (String id : rglist) {
							if (nrg.equalsIgnoreCase(id)) return;
						}
					}
				}
			}
		}
		String name = block.getType().toString();
		String name2 = block.getType().toString();
		if (conf.noblock != null && conf.noblock.length > 0) { //проверка на блоки, которые нельзя заменять
			for (String n : conf.noblock) {
				if (name.equalsIgnoreCase(n)) return;
				else if (name2.equalsIgnoreCase(n)) return;
			}
		}
		Inventory inv = player.getInventory();
		ArrayList<ItemStack> list = new ArrayList<ItemStack>();
		for (ItemStack item : inv.getContents()) { //заполнение листа с пропуском воздуха
			if (item == null || item.getType().equals(Material.AIR)) continue;
			list.add(item);
		}
		if (list.isEmpty()) return; //не создавать пустые сундуки
		block.setType(Material.CHEST); //создание сундука
		Inventory chestinv = ((Chest) block.getState()).getInventory();
		int max = chestinv.getMaxStackSize();
		for (int i = 0; i < list.size(); i++) { //заполнение сундука
			if (i < chestinv.getSize()) {
				ItemStack item = list.get(i);
				if (item.getAmount() <= 0 || item.getAmount() > max) continue; //полезная проверочка
				chestinv.addItem(item);
				player.getInventory().remove(item); //удаление предмета из инвентаря и дропа
				event.getDrops().remove(item);
			} else break;
		}
		block.getState().update(true, true);
		block2.setType(Material.SIGN_POST); //установка таблички
		Sign sign = (Sign) block2.getState();
		sign.setLine(0, player.getDisplayName());
		sign.setLine(1, d.format(new Date()));
		sign.update();
		String cords = getLoc(loc);
		player.sendMessage(conf.mess.replaceAll("%loc%", cords)); //уведомления
		if (conf.cons) Bukkit.getLogger().info(player.getName() + " умер на " + cords);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender.hasPermission("df.reload")) {
			if (args.length > 0) {
				if (!args[0].equalsIgnoreCase("reload")) return false;
				conf.load();
				sender.sendMessage(conf.reload);
			} else sender.sendMessage(conf.help);
		} else sender.sendMessage(conf.noperm);
		return true;
	}
}
